Screen <- tkinter.Tk()
FUNCTION Screen:
    Screen.title("Airport Simulation Program")
    Screen.size("1300x850")
    Screen.icon("Airplanebitmap.ico")
    BackgroundStart <- PhotoImage(file="TimeofdayBackgrounds\Map12.png")
    BackgroundStartlabel <- Label(image=BackgroundStart)
    BackgroundStartlabel.place(x=0, y=0)
    ButtonLabel <- Label
    ButtonLabel.place(x=1000, y=660)
    ButtonStart <- Button
    ButtonExit <- Button
    ButtonStart.place(row=0, column=0)
    ButtonExit.place(row=1, column=0)
    mainloop()
ENDFUNCTION

FUNCTION Initiate():
    Global <- B,BSet
    B <- {0:PhotoImage(file="TimeofdayBackgrounds\Map12.png"),
         1:PhotoImage(file="TimeofdayBackgrounds\Map13.png"),
         2:PhotoImage(file="TimeofdayBackgrounds\Map14.png"),
         3:PhotoImage(file="TimeofdayBackgrounds\Map15.png"),
         4:PhotoImage(file="TimeofdayBackgrounds\Map16.png"),
         5:PhotoImage(file="TimeofdayBackgrounds\Map17.png"),
         6:PhotoImage(file="TimeofdayBackgrounds\Map18.png"),
         7:PhotoImage(file="TimeofdayBackgrounds\Map19.png"),
         8:PhotoImage(file="TimeofdayBackgrounds\Map20.png"),
         9:PhotoImage(file="TimeofdayBackgrounds\Map21.png"),
         10:PhotoImage(file="TimeofdayBackgrounds\Map22.png"),
         11:PhotoImage(file="TimeofdayBackgrounds\Map23.png"),
         12:PhotoImage(file="TimeofdayBackgrounds\Map24.png")}
    BSet <- B[12]
    Backgroundlabel <- Label(Screen, image=BSet)
    Backgroundlabel.place(x=0, y=0)
    SliderLabel <- Label(Screen, bg="#000000")
    SliderLabel.place(x=1000, y=750)
    Slider <- Scale
    Slidername <- SliderLabel
    Slidername.place(row=0, column=0)
    Slider.place(row=1, column=0)
    mainloop()
ENDFUNCTION

FUNCTION Timeofday(Slider):
    Time <- int(Slider)
    for count in range(0,13):
        IF Time = count:
            count <- 12-count
            BSet <- B[count]
        ELSE:
            pass
        ENDIF
    ENDFOR
    for count in range(13,25):
        IF Time = count:
            BSet <- B[count-12]
        ENDIF
    ENDFOR
    Backgroundlabel <- Label(image=BSet)
    Backgroundlabel.place(x=0, y=0)
ENDFUNCTION

CLASS Plane:
    FUNCTION __init__(self,Planetype,Noofpassengers,Origin,Destination,Traveltime):
         Planetype <- Planetype
         Noofpassengers <- Noofpassengers
         Origin <- Origin
         Destination <- Destination
         Traveltime <- Traveltime
    ENDFUNCTION

    FUNCTION Alterflighttime(self,timechange):
         Traveltime <- timechange
    ENDFUNCTION

    FUNCTION Alterflightdest(self,Dest):
         Destination <- Dest
    ENDFUNCTION

ENDCLASS

Plane1 <- Plane("A380",190,"LHT","STD",180)
Plane2 <- Plane("A380",190,"LHT","STD",180)
Plane1.Alterflightdest("MAP")
Plane1.Alterflighttime("192")
Plane2.Alterflightdest("BHM")
OUTPUT Plane1.__dict__
OUTPUT Plane2.__dict__
Screen
