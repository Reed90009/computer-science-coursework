#flight program
from tkinter import *
import tkinter
import time
import sys
import threading
from datetime import timedelta

Screen = tkinter.Tk()
def loadingscreenTk(Screen):
    Screen.title("Airport Simulation Program")
    Screen.geometry("1300x850")
    Screen.wm_iconbitmap("Airplanebitmap.ico")
    BackgroundStart = tkinter.PhotoImage(file="TimeofdayBackgrounds\Map24.png")
    BackgroundStartlabel = tkinter.Label(Screen, image=BackgroundStart)
    BackgroundStartlabel.place(x=-2, y=-2)
    coverlabel = tkinter.Label(height=850, width=420, bg="#000000")
    coverlabel.place(x=882, y=0)
    ButtonLabel = tkinter.Label(Screen, bg="#000000")
    ButtonLabel.place(x=950 , y=660)
    ButtonStart = tkinter.Button(ButtonLabel, text="Start Simulation", fg="#a1dbcd", bg="#383a39", command=Initiate, height=3, width=40)
    ButtonExit = tkinter.Button(ButtonLabel, text="Exit Simulation", fg="#a1dbcd", bg="#383a39", command=Screen.destroy, height=3, width=40)
    ButtonStart.grid(row=0, column=0)
    ButtonExit.grid(row=1, column=0)
    Screen.mainloop()

def Initiate():
    global B,BSet,Airicon,PLocation,Time
    Airicon = tkinter.PhotoImage(file="Airicon.png")
    B = {0:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map12.png"),
         1:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map13.png"),
         2:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map14.png"),
         3:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map15.png"),
         4:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map16.png"),
         5:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map17.png"),
         6:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map18.png"),
         7:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map19.png"),
         8:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map20.png"),
         9:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map21.png"),
         10:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map22.png"),
         11:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map23.png"),
         12:tkinter.PhotoImage(file="TimeofdayBackgrounds\Map24.png")}
    BSet = B[12]
    Location = {"DUB":[240,260],
                "MAN":[312,270],
                "LTN":[],
                "CDG":[],
                "BRU":[],}
    PLocation = [240,260]
    Backgroundlabel = tkinter.Label(Screen, image=BSet)
    Backgroundlabel.place(x=-2, y=-2)
    coverlabel = tkinter.Label(height=850, width=420, bg="#000000")
    coverlabel.place(x=882, y=0)
    SliderLabel = tkinter.Label(Screen, bg="#000000", padx="2", pady="2")
    SliderLabel.place(x=885, y=700)
    SetTime = tkinter.Scale(SliderLabel, from_=0, to=24, orient=tkinter.HORIZONTAL, fg="#a1dbcd", bg="#383a39", length=400, command=Timeofday)
    SetSpeed = tkinter.Scale(SliderLabel, from_=0, to=5, orient=tkinter.HORIZONTAL, fg="#a1dbcd", bg="#383a39", length=400, command=RunSpeed)
    SetTimename = tkinter.Label(SliderLabel, text="Hour of Day", fg="#a1dbcd", bg="#383a39")
    SetSpeedname = tkinter.Label(SliderLabel, text="Set Speed", fg="#a1dbcd", bg="#383a39")
    SetTimename.grid(row=0, column=0)
    SetTime.grid(row=1, column=0)
    SetSpeedname.grid(row=2, column=0)
    SetSpeed.grid(row=3, column=0)
    
def Timeofday(Slider):
    global PLocation, Time
    Time = int(Slider)
    for count in range(0,13):
        if Time == count:
            count = 12-count
            BSet = B[count]
    for count in range(13,25):
        if Time == count:
            BSet = B[count-12]
    Backgroundlabel = tkinter.Label(Screen, image=BSet)
    Backgroundlabel.place(x=-2, y=-2)
    Airplanelabel = tkinter.Label(Screen, image=Airicon, text="Air 1", compound=LEFT, width=28, height=5, bg="#0000FF", padx=2, pady=2)
    Airplanelabel.place(x=PLocation[0], y=PLocation[1])
    PLocation[0]+= 5
    PLocation[1]+= 5

def RunSpeed(Slider):
    if int(Slider) == 0:
        Thrd.Stop = True
    if int(Slider) != 0:
        Thrd.Stop = False
    if int(Slider) == 1:
        Thrd.Speed = 0.5
    if int(Slider) == 2:
        Thrd.Speed = 0.005
    if int(Slider) == 3:
        Thrd.Speed = 0.00005
    if int(Slider) == 4:
        Thrd.Speed = 0.000005
    if int(Slider) == 5:
        Thrd.Speed = 0.0000005

class Plane:
    def __init__(self,Planetype,Noofpassengers,Origin,Destination,Traveltime):
        self.Planetype = Planetype
        self.Noofpassengers = Noofpassengers
        self.Origin = Origin
        self.Destination = Destination
        self.Traveltime = Traveltime

    def Alterflighttime(self,timechange):
        self.Traveltime = timechange

    def Alterflightdest(self,Dest):
        self.Destination = Dest

class ClockThread(threading.Thread):
    def __init__(self,Time,Speed,Stop,Hour):
        threading.Thread.__init__(self)
        self.Time = Time
        self.Speed = Speed
        self.Stop = Stop
        self.Hour = Hour

    def run(self):
        Time = self.Time
        while True:
            Speed = self.Speed
            time.sleep(Speed)
            Stop = self.Stop
            if Stop == False:
                if Speed==0.0000005:
                    Time+=121
                elif Speed<=0.00005:
                    Time+=15
                else:
                    Time += 1
                self.Time = Time
            else:
                pass
            if Time>=86400:
                Coverlabel = tkinter.Label(Screen, width=13, height=5, bg="#000000")
                Coverlabel.place(x=885,y=50)
                Time = 0
            else:
                pass
            CurrentTime=timedelta(seconds=Time)
            self.Hour=CurrentTime.days(seconds=Time)
            Backgroundlabel = tkinter.Label(Screen, text=CurrentTime, font=("Helvetica", 40), width=13)
            Backgroundlabel.place(x=885, y=50)
            


Plane1 = Plane("A380",190,"LHT","STD",180)
Plane2 = Plane("A380",190,"LHT","STD",180)
Plane1.Alterflightdest("MAP")
Plane1.Alterflighttime("192")
Plane2.Alterflightdest("BHM")
print(Plane1.__dict__)
print(Plane2.__dict__)
Thrd = ClockThread(0,0,True,0)
Thrd.start()
print(Thrd.Time)
loadingscreenTk(Screen)
