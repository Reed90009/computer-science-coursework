#flight program
import Tkinter
import pygame
import time
import sys

Screen = Tkinter.Tk()
def loadingscreenTk(Screen):
    Screen.title("Airport Simulation Program")
    Screen.geometry("1301x833")
    Screen.wm_iconbitmap("Airplanebitmap.ico")
    Background = Tkinter.PhotoImage(file="Europe_busiest_airports.png")
    Backgroundlabel = Tkinter.Label(Screen, image=Background)
    Backgroundlabel.place(x=0, y=0, relwidth=1, relheight=1)
    ButtonLabel = Tkinter.Label(Screen, bg="#000000")
    ButtonLabel.place(x=1000, y=660)
    ButtonStart = Tkinter.Button(ButtonLabel, text="Start Simulation", fg="#a1dbcd", bg="#383a39", command=Initiate, height=3, width=40)
    ButtonExit = Tkinter.Button(ButtonLabel, text="Exit Simulation", fg="#a1dbcd", bg="#383a39", command=Screen.destroy, height=3, width=40)
    ButtonStart.grid(row=0, column=0)
    ButtonExit.grid(row=1, column=0)
    Screen.mainloop()

def Initiate():
    Background = Tkinter.PhotoImage(file="Europe_busiest_airports.png")
    Background1 = Tkinter.PhotoImage(file="Europe_busiest_airportsMorning.png")
    Background2 = Tkinter.PhotoImage(file="Europe_busiest_airportsMidnight.png")
    BackgroundSet = Background
    SliderLabel = Tkinter.Label(Screen, bg="#000000")
    SliderLabel.place(x=1000, y=750)
    Slider = Tkinter.Scale(SliderLabel, from_=0, to=24, orient=tkinter.HORIZONTAL, fg="#a1dbcd", bg="#383a39", command=Timeofday)
    Slidername = Tkinter.Label(SliderLabel, text="Hour of Day", fg="#a1dbcd", bg="#383a39")
    Backgroundlabel = Tkinter.Label(Screen, image=BackgroundSet)
    Backgroundlabel.place(x=0, y=0, relwidth=1, relheight=1)
    Slidername.grid(row=0, column=0)
    Slider.grid(row=1, column=0)
    Backgroundlabel.image = Background
    Screen.mainloop()

def Timeofday(Slider):
    Time = int(Slider)
    if Time >= 12 or Time < 12 or Time == 0:
        BackgroundSet = Background
    elif Time > 18 or Time < 9:
        BackgroundSet = Background1
    elif Time > 21 or Time < 4:
        BackgroundSet = Background2
    print(Background)
#loadingscreen()
loadingscreenTk(Screen)
#pygame.display.update()

